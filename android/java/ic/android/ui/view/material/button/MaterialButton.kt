package ic.android.ui.view.material.button


import ic.graphics.color.ColorArgb

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup.LayoutParams.MATCH_PARENT

import ic.android.ui.view.View
import ic.android.ui.view.ext.setOnClickAction
import ic.android.ui.view.material.MaterialView
import ic.android.ui.view.setForegroundFromResource
import ic.gui.material.R


@Suppress("LeakingThis")
open class MaterialButton : MaterialView {


	val backgroundView = View(context).also { addView(it, MATCH_PARENT, MATCH_PARENT) }


	inline fun setOnClickAction (crossinline onClick : () -> Unit) = backgroundView.setOnClickAction(onClick)


	var colorArgb : ColorArgb = ColorArgb(0xff000000)
		set(value) {
			field = value
			backgroundView.setBackgroundColor(value)
		}
	;


	var isActive : Boolean = true
		set(value) {
			field = value
			backgroundView.isClickable = value
			backgroundView.isFocusable = value
		}
	;


	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads
	constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : super (context, attrs, defStyleAttr) {

		if (attrs != null) {

			val styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.MaterialButton)

			colorArgb = styledAttributes.getColor(R.styleable.MaterialButton_color, colorArgb)

			backgroundView.setForegroundFromResource(ic.base.R.drawable.ripple_white)

			styledAttributes.recycle()

		}

	}


}