package ic.android.ui.view.material.checkbox


import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView

import ic.android.ui.view.ext.setOnClickAction
import ic.android.ui.view.layout.frame.FrameLayout
import ic.android.ui.view.image.ext.setTintColor
import ic.android.util.units.dpToPx
import ic.base.primitives.bool.ext.then
import ic.graphics.color.ColorArgb
import ic.gui.material.R


class MaterialCheckbox : FrameLayout {


	private val imageView = ImageView(context).apply {
		scaleType = ImageView.ScaleType.CENTER
	}

	init {
		addView(
			imageView,
			LayoutParams(
				dpToPx(48),
				dpToPx(48)
			)
		)
	}


	var isChecked : Boolean = false
		set (value) {
			field = value
			updateUi()
			val isInvokedByUserInteraction = this.isInvokedByUserInteraction
			this.isInvokedByUserInteraction = false
			onCheckedStateChangedAction(value, isInvokedByUserInteraction)
		}
	;


	private fun updateUi() {
		imageView.setImageResource(
			isChecked.then(
				ic.res.icons.material.R.drawable.icon_checkbox_checked_24dp,
				ic.res.icons.material.R.drawable.icon_checkbox_unchecked_24dp,
			)
		)
	}


	var colorArgb : ColorArgb = ColorArgb(0xff000000)
		set(value) {
			field = value
			imageView.setTintColor(value)
		}
	;


	private var onCheckedStateChangedAction
		: (isChecked: Boolean, isCalledByUserInteraction: Boolean) -> Unit
		= { _, _ -> }
	;

	private var isInvokedByUserInteraction : Boolean = false

	fun setOnCheckedStateChangedAction (
		toCallAtOnce : Boolean = false,
		onIsCheckedChangedAction : (isChecked: Boolean, isInvokedByUserInteraction: Boolean) -> Unit
	) {
		this.onCheckedStateChangedAction = onIsCheckedChangedAction
		if (toCallAtOnce) {
			onIsCheckedChangedAction(isChecked, false)
		}
	}


	init {

		isClickable = true

		setOnClickAction {
			isInvokedByUserInteraction = true
			isChecked = !isChecked
		}

	}


	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads
	constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : super (context, attrs, defStyleAttr) {
		if (attrs != null) {
			val styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.MaterialCheckbox)
			colorArgb = styledAttributes.getColor(R.styleable.MaterialCheckbox_color, colorArgb)
			styledAttributes.recycle()
		}
	}


}