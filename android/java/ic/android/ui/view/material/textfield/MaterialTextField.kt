package ic.android.ui.view.material.textfield


import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.View
import ic.android.storage.res.getResFontOrNull

import ic.android.ui.view.ext.*
import ic.android.ui.view.layout.frame.FrameLayout
import ic.android.ui.view.text.ext.textColorArgb
import ic.android.ui.view.text.ext.value
import ic.android.ui.view.textinput.TextInputMode
import ic.android.ui.view.edittext.ext.inputMode
import ic.android.ui.view.group.ext.inflateContent
import ic.android.ui.view.text.TextView
import ic.android.ui.view.textinput.TextInputView
import ic.gui.anim.animateValue
import ic.base.primitives.bool.ext.then
import ic.base.primitives.float32.ext.asInt32
import ic.base.primitives.float32.Float32
import ic.base.primitives.int32.ext.asFloat32
import ic.base.strings.ext.isEmpty
import ic.design.task.Task
import ic.graphics.color.Color
import ic.math.denormalize
import ic.graphics.color.ColorArgb
import ic.graphics.color.ext.toArgb
import ic.gui.dim.dp.ext.dp
import ic.gui.dim.dp.ext.inPx
import ic.gui.dim.px.Px
import ic.math.denormalizeToColorArgb

import ic.gui.material.R


class MaterialTextField : FrameLayout {

	private val materialFieldRootView : View          get() = findViewById(R.id.materialFieldRootView)
	private val outlineView           : View          get() = findViewById(R.id.outlineView)
	        val textInputView         : TextInputView get() = findViewById(R.id.textInputView)
	private val hintView              : TextView      get() = findViewById(R.id.hintView)

	private var privateAccentColorArgb : ColorArgb = ColorArgb(0xff000000)
	var accentColorArgb : ColorArgb
		get() = privateAccentColorArgb
		set(value) {
			privateAccentColorArgb = value
			updateOutline()
			updateAnimationPhase()
		}
	;

	private var privateTextColorArgb : ColorArgb = ColorArgb(0xff000000)
	var textColorArgb : ColorArgb
		get() = privateTextColorArgb
		set(value) {
			privateTextColorArgb = value
			updateAnimationPhase()
		}
	;

	private var privateHintColorArgb : ColorArgb = ColorArgb(0xff808080)
	var hintColorArgb : ColorArgb
		get() = privateHintColorArgb
		set(value) {
			privateHintColorArgb = value
			updateAnimationPhase()
		}
	;

	var backgroundColorArgb : ColorArgb = Color.White.toArgb()
		set(value) {
			field = value
			hintView.setBackgroundColor(field)
		}
	;

	var font : Typeface? = null
		set(value) {
			field = value
			textInputView.typeface = value
			hintView.typeface = value
		}
	;

	var hint : String
		get() = hintView.value
		set(value) { hintView.value = value }
	;

	var value : String
		get() = textInputView.value
		set(value) { textInputView.value = value }
	;

	private var privateHeightPx : Px = 48.dp.inPx
	var heightPx : Px
		get() = privateHeightPx
		set(value) {
			privateHeightPx = value
			updateHeight()
			updateAnimationPhase()
		}
	;

	fun setOnValueChangedAction (
		toCallAtOnce : Boolean = false,
		onValueChanged : (oldValue: String, newValue: String, isInvokedByUserInteraction: Boolean) -> Unit
	) {
		textInputView.setOnValueChangedAction(toCallAtOnce = toCallAtOnce, onValueChanged = onValueChanged)
	}

	private fun updateHeight() {
		materialFieldRootView.layoutHeightPx = privateHeightPx
	}

	private fun updateOutline() {
		outlineView.setBackgroundRoundCorners(
			strokeColor = privateAccentColorArgb,
			cornersRadius = 8.dp,
			strokeWidth = 2.dp
		)
	}

	private var animateTask : Task? = null

	private var animationPhase : Float32 = Float32(0)

	private fun updateAnimationPhase() {
		hintView.scale = animationPhase.denormalize(1, .75)
		hintView.textColorArgb = animationPhase.denormalizeToColorArgb(privateHintColorArgb, privateAccentColorArgb)
		hintView.translationXDp = animationPhase.denormalize(0, 8)
		hintView.translationYPx = animationPhase.denormalize(0, -privateHeightPx / 2).asInt32
	}

	private fun updateIsEmptyState() {
		animateTask?.cancel()
		animateTask = animateValue(
			toAnimate = isAttachedToWindow,
			from = animationPhase,
			to = textInputView.value.isEmpty.then(0, 1).asFloat32,
			onStop = { animateTask = null }
		) { animationPhase ->
			this.animationPhase = animationPhase
			updateAnimationPhase()
		}
	}

	override fun onDetachedFromWindow() {
		super.onDetachedFromWindow()
		if (animateTask != null) {
			updateIsEmptyState()
		}
	}

	inline fun setOnClickAction (crossinline action: () -> Unit) {
		textInputView.isLocked = true
		textInputView.isFocusable = false
		isClickable = true
		isFocusable = true
		(this as View).setOnClickAction(action)
	}

	@Suppress("ConvertSecondaryConstructorToPrimary")
	@JvmOverloads
	constructor (context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : super(context, attrs, defStyleAttr) {

		clipChildren 	= false
		clipToPadding 	= false

		inflateContent(R.layout.material_textfield)

		textInputView.addOnValueChangedAction { oldValue, newValue, _ ->
			if (oldValue.isEmpty != newValue.isEmpty) {
				updateIsEmptyState()
			}
		}

		if (attrs != null) {

			val styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.MaterialTextField)

			privateHeightPx = styledAttributes.getDimensionPixelSize(R.styleable.MaterialTextField_height, privateHeightPx)

			privateAccentColorArgb 	= styledAttributes.getColor(R.styleable.MaterialTextField_accentColor, privateAccentColorArgb)
			privateTextColorArgb 	= styledAttributes.getColor(R.styleable.MaterialTextField_textColor, privateTextColorArgb)
			privateHintColorArgb 	= styledAttributes.getColor(R.styleable.MaterialTextField_hintColor, privateHintColorArgb)

			backgroundColorArgb = styledAttributes.getColor(R.styleable.MaterialTextField_backgroundColor, backgroundColorArgb)

			val fontResourceId = styledAttributes.getResourceId(R.styleable.MaterialTextField_font, -1)
			if (fontResourceId != -1) {
				font = getResFontOrNull(fontResourceId)
			}

			hint = styledAttributes.getString(R.styleable.MaterialTextField_hint) ?: ""

			textInputView.inputMode = TextInputMode.byId(
				styledAttributes.getInt(R.styleable.MaterialTextField_textInputMode, 0)
			)

			styledAttributes.recycle()

		}

		updateOutline()
		updateHeight()
		updateIsEmptyState()

	}

}
