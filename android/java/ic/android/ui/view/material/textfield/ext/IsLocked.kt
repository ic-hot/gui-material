package ic.android.ui.view.material.textfield.ext


import ic.android.ui.view.material.textfield.MaterialTextField


inline var MaterialTextField.isLocked : Boolean

	get() = this.textInputView.isLocked

	set(value) {
		this.textInputView.isLocked = value
	}

;